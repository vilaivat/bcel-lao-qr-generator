using System;
using System.Text;


public class HelloWorld
{
    public static void Main(string[] args)
    {
        //variable from Bank
        string mcid = "mch5949fa044ed9e"; // mcid = mch5949fa044ed9e this data get from BCEL Bank
        string amount = "2555"; // amount = 2555 for this QR code
        string mcc = "5732";  // merchant category code get from BCEL Bank
        
        //variable merchant
        string terminal = "0001"; // terminal or counter ID
        string unqinue = generateID(); // system auto generate, this value need to add to DB to check transaction complete or not?
        string billno = unqinue; // bill number ID
        string merchantname = "Bunny test";

        string tag00 = buildMessage("00", "01");
        string tag01 = buildMessage("01", "12");

        string tag3800 = buildMessage("00", "A005266284662577");
        string tag3801 = buildMessage("01", "27710418");
        string tag3802 = buildMessage("02", "002");
        string tag3803 = buildMessage("03", mcid);
        string tag38 = buildMessage("38", tag3800 + tag3801 + tag3802 + tag3803);
        
        string tag52 = buildMessage("52", mcc);
        string tag53 = buildMessage("53", "418");
        string tag54 = buildMessage("54", amount);
        string tag58 = buildMessage("58", "LA");
        string tag59 = buildMessage("59", merchantname);
        string tag60 = buildMessage("60", "VTE");

        string tag6201 = buildMessage("01", billno);
        string tag6205 = buildMessage("05", unqinue);
        string tag6207 = buildMessage("07", terminal);
        string tag6208 = buildMessage("08", "Orange Juice");
        string tag62 = buildMessage("62", tag6201 + tag6205 + tag6207 + tag6208);

        string tag63 = "6304";// checksum length

        string checksum = CalcCRC16(tag00 + tag01 + tag38 + tag52 + tag53 + tag54 + tag58 + tag59 + tag60 + tag62 + tag63);
        string emvqr = tag00 + tag01 + tag38 + tag52 + tag53 + tag54 + tag58 + tag59 + tag60 + tag62 + tag63 + checksum;

        Console.WriteLine (emvqr);
    }
    
    static string buildMessage(string tag, string value)
    {
        string leng = value.Length.ToString("D2");
        return tag + leng + value;
    }

    static string CalcCRC16(string strchecksum)
    {
        byte[] data = Encoding.ASCII.GetBytes(strchecksum);
        ushort crc = 0xFFFF;
        for (int i = 0; i < data.Length; i++)
        {
            crc ^= (ushort)(data[i] << 8);
            for (int j = 0; j < 8; j++)
            {
                if ((crc & 0x8000) > 0)
                    crc = (ushort)((crc << 1) ^ 0x1021);
                else
                    crc <<= 1;
            }
        }
        return crc.ToString("X4");
    }
    static string generateID()
    {
        return Guid.NewGuid().ToString("N");
    }

}