<?php

// Bank provide these data
$receiver_id = "mch5949fa044ed9e"; // merchant id generate by BCEL Onepay system
$merchant_name = "Bunny test";
$mcc = "5732";

// You set these data
$amount = 2555; // service amount in ccy current
$invoiceid = uniqid(); // bill number 
$uuid = $invoiceid; //uuid = unique id is string 1-99 char
$terminalid = "0001";// terminal is string 1-99 char
$purpose = "Orange Juice"; 

if (!$uuid) exit("Please set uuid in GET parameter");

function buildqr($arr){
	$res = "";
	foreach ($arr as $key => $val){
		if (!$val) continue;
		$res .= str_pad($key, 2, "0", STR_PAD_LEFT) .
				str_pad(strlen($val), 2, "0", STR_PAD_LEFT) . 
				$val;
	}
	return $res;
}

function crc16($sStr, $aParams = array()){
	$aDefaults = array(
		"polynome" => 0x1021,
		"init" => 0xFFFF,
		"xor_out" => 0,
	);
	foreach ($aDefaults as $key => $val){
		if (!isset($aParams[$key])){
		$aParams[$key] = $val;
		}
	}
	$sStr .= "";
	$crc = $aParams['init'];
	$len = strlen($sStr);
	$i = 0;
	while ($len--){
		$crc ^= ord($sStr[$i++]) << 8;
		$crc &= 0xffff;
		for ($j = 0; $j < 8; $j++){
			$crc = ($crc & 0x8000) ? ($crc << 1) ^ $aParams['polynome'] :
			$crc << 1;
			$crc &= 0xffff;
		}
	}
	$crc ^= $aParams['xor_out'];
	return str_pad(strtoupper(dechex($crc)),4, "0", STR_PAD_LEFT);
}

$rawqr = buildqr([
	00 => "01",
	01 => "12",
	38 => buildqr([
		00 => "A005266284662577", // AID 
		01 => "27710418", //IIN
		02 => "002", // Payment type
		03 => $receiver_id //Receiver ID
	]),
	52 => $mcc,
	53 => "418",
	54 => $amount,
	58 => "LA",
	59 => $merchant_name,
	60 => "VTE",
	62 => buildqr([
		01 => $invoiceid,
		05 => $uuid,
		07 => $terminalid,
		8 => $purpose
	])
]);

$fullqr = $rawqr . buildqr([63 => crc16($rawqr . "6304")]); // 6304 is from system no need to change

?>

<p><img src="https://api.qrserver.com/v1/create-qr-code/?size=150x150&ecc=H&data=<?php echo $fullqr; ?>" width="200" height="200" /></p>
<p><?php echo $fullqr; ?></p>