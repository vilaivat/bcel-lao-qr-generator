<?php
    $data = file_get_contents('php://input');
    $request_method = $_SERVER['REQUEST_METHOD'];

    //get header
    $headers = getallheaders();
    $signature = $headers['Signature'];

    if(!(isset($data) && $data!="" && isset($signature) && $signature!="" )) {
        echo json_encode([
            'status' => '1', 
            'message' => 'Signature or data is missing'
        ]);
        return;
    }

    //-------------------- Convert the HEX public key to binary format ------------------
    $public_key_hex = "30820122300D06092A864886F70D01010105000382010F003082010A0282010100BB8E71F82ACF2D48010D9E728D9B9512E8D6F024E4CE305462B8D652345A044A59A587590E9BEAC3AE40BC5B0FC5B078E4C9C3B10514D81A2DE37B32590F3CDB4EE7852296D177FF9BB3473E611FD219B96180B77804542C7D569A320FAD9B8EA84A5D5AB8A058693428A35E7E45FBBAAB419B0133B16A8D5FC1989B7FADB5D65D336A94C5FCAC3E29E8AEB71C9037AB154E8A727328A6A02E15499EFD91291D960AC3C22AAF7E8FEC82553CE4547E18304F910D12182B793B00FAC6D322956E75BE921860B0CFD76817DE6B267D5BE75734F9F468573FA20D6869DD821C103EC4F45B14A70F2248194F1E4D6FB736BB58B92F15321D91C2F82867AC06C3C1D70203010001"; // Replace with the actual HEX public key

    $public_key_bin = hex2bin($public_key_hex); // Create a DER-encoded public key
    $der_public_key = $public_key_bin; // Convert the DER-encoded public key to PEM format
    $public_key = "-----BEGIN PUBLIC KEY-----\r\n" . chunk_split(base64_encode($der_public_key), 64)."-----END PUBLIC KEY-----";
    
    //-------------------- collect log data ------------------
    savetolog("Method: ". $request_method .", Signature: ". $signature."\nData: ". $data);

    //-------------------- Verify the signature ------------------
    $result = openssl_verify($data, hex2bin($signature), $public_key, OPENSSL_ALGO_SHA256);
    savetolog("openssl_verify: ". $result);

    header('Content-Type: application/json');
    if ($result == 1) {
        echo json_encode([
            'status' => '0', 
            'message' => 'transaction saved.'
        ]);
        return;
    } elseif ($result == 0) {
        echo json_encode([
            'status' => '1', 
            'message' => 'Signature is invalid'
        ]);
        return;
    } else {
        echo json_encode([
            'status' => '1', 
            'message' => 'Error verifying signature'
        ]);
        return;
    }

    //-------------------- function ------------------
    function savetolog($log) {
        $filename = 'logs/callback_'.date("Ymd").'.log';
        $handle = fopen($filename, 'a+') or die('Cannot open file:  '.$filename);
        fwrite($handle, date("Y-m-d H:i:s").$log."\n");
        fclose($handle);
    }