import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.lang.String;

class QrParser {
    
    public static class QrField {
        public String key;
        public String value;
        public List<QrField> subfields;
    
        public QrField(){}
        public QrField(String key, String value){
            this.key = key;
            this.value = value;
        }
        public QrField(String key, List<QrField> subfields){
            this.key = key;
            this.subfields = subfields;
        }
    }
    
    public static void main(String[] args) {
        try {
            //variable from Bank
            String mcid = "mch5949fa044ed9e";
            String mcc = "5732";
            String merchantName = "Bunny test";
            
            //variable merchant
            String invoiceId = "INVOICETEST0056"; // running number generate by merchant
            String purpose = "payment for INVOICETEST0056";
            String terminalid = "0001"; // terminal ID number each counter
            BigDecimal amount = new BigDecimal("2555");
    
            String qr = QrParser.buildQr(Arrays.asList(
                new QrField("00", "01"),
                new QrField("01", "12"),
                new QrField("38", Arrays.asList(
                        new QrField("00", "A005266284662577"),
                        new QrField("01", "27710418"),
                        new QrField("02", "002"),
                        new QrField("03", mcid)
                )),
                new QrField("52", mcc),
                new QrField("53", "418"),
                new QrField("54", amount.toPlainString()),
                new QrField("58", "LA"),
                new QrField("59", merchantName),
                new QrField("60", "VTE"),
                new QrField("62", Arrays.asList(
                        new QrField("01", invoiceId),
                        new QrField("05", invoiceId),
                        new QrField("07", terminalid),
                        new QrField("08", purpose)
                ))
            ));
            // CRC-16/CCITT-FALSE
            String crc = QrParser.crc16(qr + "6304");
            qr = qr + "6304" + crc;
            
            System.out.println("QR with crc :" + qr+ "\n");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String buildQr(List<QrField> fields) throws Exception {
        StringBuilder qr = new StringBuilder();
        for (QrField q : fields) {
            if (q.value == null && q.subfields == null) continue;
            String value = q.subfields != null && q.subfields.size() > 0 ? buildQr(q.subfields) : q.value;
            if (value.length() == 0) continue;
            if (value.length() > 99)
                throw new Exception("QR field " + q.key + " is longer than 99 characters");
            qr.append(q.key)
                    .append(padZeroToNumber(value.length()))
                    .append(value);
        }
        return qr.toString();
    }

    private static String padZeroToNumber(int num) {
        String res = "" + num;
        if (res.length() == 1) res = "0" + res;
        return res;
    }
    
    public static String crc16(String data) {
        byte[] bytes = data.getBytes();
        int crc = 0xFFFF;          // initial value
        int polynomial = 0x1021;   // 0001 0000 0010 0001  (0, 5, 12)
        for (byte b : bytes) {
            for (int i = 0; i < 8; i++) {
                boolean bit = ((b   >> (7-i) & 1) == 1);
                boolean c15 = ((crc >> 15    & 1) == 1);
                crc <<= 1;
                if (c15 ^ bit) crc ^= polynomial;
            }
        }
        crc &= 0xffff;
        return Integer.toHexString(crc).toUpperCase();
    }
}